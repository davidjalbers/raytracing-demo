/*
 *  3D Raytracing Demo Project 2.0
 *
 *  Autor: David Albers.
 */

package de.davidjalbers.rtd2.scene.camera;

import de.davidjalbers.rtd2.scene.generic.DirectionableSceneObjectBase;
import de.davidjalbers.rtd2.scene.Ray;
import de.davidjalbers.rtd2.scene.Vector3f;

/**
 * Eine Kamera, die keine perspektivischen Verzerrungen berücksichtigt. Weit entfernte Objekte erscheinen dadurch genauso groß wie nahe.
 */
public class OrthograficCamera extends DirectionableSceneObjectBase implements ICamera {


    private final float zoom;

    /**
     * Erzeugt eine neue orthografische Kamera mit einer Position und einer Blickrichtung.
     *
     * @param pos die Position der Kamera
     * @param dir die Blickrichtung der Kamera
     * @param zoom der Vergrößerungsfaktor
     */
    public OrthograficCamera(Vector3f pos, Vector3f dir, float zoom) {
        super(pos, dir);
        this.zoom = zoom;
    }

    @Override
    public Ray generateRay(int x, int y, int width, int height) {
        x = x - (width / 2);
        y = y - (height / 2);

        Vector3f widthVector = dir
                .cross(new Vector3f(0, 1, 0))
                .normalize();
        Vector3f heightVector = dir
                .cross(widthVector)
                .normalize();

        Ray ray = new Ray(pos
                .add(widthVector.multiply(x).divide(zoom)
                .add(heightVector.multiply(y).divide(zoom))), dir);
        return ray;
    }

}
