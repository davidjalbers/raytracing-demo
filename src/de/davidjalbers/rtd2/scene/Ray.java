/*
 *  3D Raytracing Demo Project 2.0
 *
 *  Autor: David Albers.
 */

package de.davidjalbers.rtd2.scene;

import de.davidjalbers.rtd2.scene.generic.DirectionableSceneObjectBase;

/**
 * Repräsentiert einen Strahl mit einem Ursprung und einer Richtung im dreidimensionalen Raum.
 */
public class Ray extends DirectionableSceneObjectBase {

    /**
     * Erzeugt einen neuen Strahl mit später nicht veränderbarem Ursprung und Richtung.
     * @param pos der Ursprung des zu erzeugenden Strahls
     * @param dir die Richtung des zu erzeugenden Strahls
     */
    public Ray(Vector3f pos, Vector3f dir) {
        super(pos, dir.normalize());
    }

}
