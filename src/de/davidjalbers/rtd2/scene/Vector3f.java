/*
 *  3D Raytracing Demo Project 2.0
 *
 *  Autor: David Albers.
 */

package de.davidjalbers.rtd2.scene;

/**
 * Ein Vektor im dreidimensionalen Raum.
 */
public class Vector3f implements Cloneable {

    public float x, y, z;

    /**
     * Erzeugt einen neuen Vektor mit den übergebenen Werten.
     * @param x der x-Wert des zu erzeugenden Vektors
     * @param y der y-Wert des zu erzeugenden Vektors
     * @param z der z-Wert des zu erzeugenden Vektors
     */
    public Vector3f(float x, float y, float z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }


    ///////////////////////////////////// STATIC METHODS ///////////////////////////////////////////////////////////////

    /**
     * Addiert zwei Vektoren.
     * @param a erster Summand
     * @param b zweiter Summand
     * @return Summe als Vektor
     */
    public static Vector3f add(Vector3f a, Vector3f b) {
        return new Vector3f(
                a.x + b.x,
                a.y + b.y,
                a.z + b.z
        );
    }

    /**
     * Subtrahiert zwei Vektoren.
     * @param a der Vektor, von dem subtrahiert werden soll (Minuend)
     * @param b der Vektor, der subtrahiert werden soll (Subtrahend)
     * @return Differenz als Vektor
     */
    public static Vector3f subtract(Vector3f a, Vector3f b) {
        return new Vector3f(
                a.x - b.x,
                a.y - b.y,
                a.z - b.z
        );
    }

    /**
     * Multipliziert einen Vektor mit einem Skalar.
     * @param a zu multiplizierender Vektor
     * @param b zu multiplizierendes Skalar
     * @return das Produkt der Skalarmultiplikation
     */
    public static Vector3f multiply(Vector3f a, float b) {
        return new Vector3f(
                a.x * b,
                a.y * b,
                a.z * b
        );
    }

    /**
     * Dividiert einen Vektor durch ein Skalar.
     * @param a zu dividierender Vektor
     * @param b zu dividierendes Skalar
     * @return der Quotient der Skalardivision
     */
    public static Vector3f divide(Vector3f a, float b) {
        return new Vector3f(
                a.x / b,
                a.y / b,
                a.z / b
        );
    }

    /**
     * Bildet das Skalarprodukt zweier Vektoren.
     * @param a Vektor a
     * @param b Vektor b
     * @return das Skalarprodukt der beiden Vektoren
     */
    public static float dot(Vector3f a, Vector3f b) {
        return (a.x * b.x) +
               (a.y * b.y) +
               (a.z * b.z);
    }

    /**
     * Bildet das Kreuzprodukt zweier Vektoren.
     * @param a Vektor a
     * @param b Vektor b
     * @return das Kreuzprodukt der beiden Vektoren
     */
    public static Vector3f cross(Vector3f a, Vector3f b) {
        return new Vector3f(
                a.y * b.z - a.z * b.y,
                a.z * b.x - a.x * b.z,
                a.x * b.y - a.y * b.x
        );
    }

    /**
     * Bestimmt die Länge eines Vektors.
     * @param a der Vektor, dessen Länge bestimmt werden soll
     * @return die Länge des Vektors
     */
    public static float magnitude(Vector3f a) {
        return (float) Math.sqrt(
                a.x * a.x +
                a.y * a.y +
                a.z * a.z
        );
    }

    /**
     * Erzeugt aus einem Vektor einen neuen Vektor mit der Länge 1, ohne dabei die Richtung des ursprünglichen Vektors zu verändern (normalisieren).
     * @param a der Vektor, der normalisiert werden soll (Länge sollte nicht 1 sein)
     * @return der normalisierte Vektor mit der Länge 1
     */
    public static Vector3f normalize(Vector3f a) {
        float magnitude = magnitude(a);
        return new Vector3f(
                a.x / magnitude,
                a.y / magnitude,
                a.z / magnitude
        );
    }

    /**
     * Kehrt die Richtung um, in die ein Vektor zeigt, indem all seine Werte negiert werden.
     * @param a der Vektor, der umgekehrt werden soll
     * @return der umgekehrte Vektor
     */
    public static Vector3f inverse(Vector3f a) {
        return new Vector3f(
                -a.x,
                -a.y,
                -a.z
        );
    }


    ///////////////////////////////// NON-STATIC METHODS ///////////////////////////////////////////////////////////////

    /**
     * Nicht-statische Version von {@link #add(Vector3f, Vector3f)}.
     */
    public Vector3f add(Vector3f b) {
        return add(this, b);
    }

    /**
     * Nicht-statische Version von {@link #subtract(Vector3f, Vector3f)}.
     */
    public Vector3f subtract(Vector3f b) {
        return subtract(this, b);
    }

    /**
     * Nicht-statische Version von {@link #add(Vector3f, Vector3f)}.
     * Im Vergleich zu {@link #subtract(Vector3f)} sind hier Subtrahend und Minuend vertauscht.
     */
    public Vector3f subtractFrom(Vector3f a) {
        return subtract(a, this);
    }

    /**
     * Nicht-statische Version von {@link #multiply(Vector3f, float)}.
     */
    public Vector3f multiply(float b) {
        return multiply(this, b);
    }

    /**
     * Nicht-statische Version von {@link #divide(Vector3f, float)}.
     */
    public Vector3f divide(float b) {
        return divide(this, b);
    }

    /**
     * Nicht-statische Version von {@link #dot(Vector3f, Vector3f)}.
     */
    public float dot(Vector3f b) {
        return dot(this, b);
    }

    /**
     * Nicht-statische Version von {@link #cross(Vector3f, Vector3f)}.
     */
    public Vector3f cross(Vector3f b) {
        return cross(this, b);
    }

    /**
     * Nicht-statische Version von {@link #magnitude(Vector3f)}.
     */
    public float magnitude() {
        return magnitude(this);
    }

    /**
     * Nicht-statische Version von {@link #normalize(Vector3f)}.
     */
    public Vector3f normalize() {
        return normalize(this);
    }

    /**
     * Nicht-statische Version von {@link #inverse(Vector3f)}.
     * @return
     */
    public Vector3f inverse() {
        return inverse(this);
    }
}
