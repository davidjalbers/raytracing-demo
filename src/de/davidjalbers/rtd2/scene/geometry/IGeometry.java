/*
 *  3D Raytracing Demo Project 2.0
 *
 *  Autor: David Albers.
 */

package de.davidjalbers.rtd2.scene.geometry;

import de.davidjalbers.rtd2.scene.Intersection;
import de.davidjalbers.rtd2.scene.Ray;

import java.awt.*;

/**
 * Eine Schnittstelle für alle geometrischen Objekte, die sich mit einem Strahl schneiden können.
 */
public interface IGeometry {

    /**
     * Ermittelt rechnerisch, ob sich dieses geometrische Objekt mit einem Strahl schneidet
     * @param ray der zu testende Strahl
     * @return {@code true}, falls dieses Objekt sich mit dem Strahl schneidet, sonst {@code false}
     */
    Intersection intersect(Ray ray);

    /**
     * Gibt die Farbe dieses geometrischen Objekts zurück.
     * @return die Farbe dieses geometrischen Objekts
     */
    Color getPrimaryColor();

}
