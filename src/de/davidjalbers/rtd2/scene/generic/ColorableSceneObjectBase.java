package de.davidjalbers.rtd2.scene.generic;

import de.davidjalbers.rtd2.scene.Vector3f;

import java.awt.*;

/**
 * Erweitert die Funktionalität von {@link SceneObjectBase} um eine Farbe.
 */
public abstract class ColorableSceneObjectBase extends SceneObjectBase {

    public final Color col;

    /**
     * Erzeugt ein neues Objekt an einer bestimmten Position mit einer bestimmten Farbe.
     * @param pos die Position des zu erzeugenden Objekts
     * @param col die Farbe des zu erzeugenden Objekts
     */
    public ColorableSceneObjectBase(Vector3f pos, Color col) {
        super(pos.x, pos.y, pos.z);
        this.col = col;
    }
}
