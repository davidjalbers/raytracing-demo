/*
 *  3D Raytracing Demo Project 2.0
 *
 *  Autor: David Albers.
 */

package de.davidjalbers.rtd2.scene.generic;

import de.davidjalbers.rtd2.scene.Vector3f;

/**
 * Eine Implementierungshilfe für alle Objekte, die sich in einer Szene befinden und eine Position haben.
 */
public abstract class SceneObjectBase {

    public final Vector3f pos;

    /**
     * Erzeugt ein neues Objekt an der gegebenen Position.
     * @param x x-Wert der Position
     * @param y y-Wert der Position
     * @param z z-Wert der Position
     */
    public SceneObjectBase(float x, float y, float z) {
        pos = new Vector3f(x, y, z);
    }

}
