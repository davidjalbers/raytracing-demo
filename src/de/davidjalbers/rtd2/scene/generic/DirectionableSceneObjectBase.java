/*
 *  3D Raytracing Demo Project 2.0
 *
 *  Autor: David Albers.
 */

package de.davidjalbers.rtd2.scene.generic;

import de.davidjalbers.rtd2.scene.Vector3f;

/**
 * Erweitert die Funktionalität von {@link SceneObjectBase} um eine Richtung.
 */
public abstract class DirectionableSceneObjectBase extends SceneObjectBase {

    public final Vector3f dir;

    /**
     * Erzeugt ein neues Objekt mit einer gegebenen Position und Richtung.
     * @param pos die Position des neuen Objekts
     * @param dir die Richtung des neuen Objekts
     */
    public DirectionableSceneObjectBase(Vector3f pos, Vector3f dir) {
        super(pos.x, pos.y, pos.z);
        this.dir = dir;
    }
}
